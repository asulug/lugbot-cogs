# lugbot-cogs
Cogs for the ASULUG Discord bot

# Installation
Provide installation instructions and possible 3rd party requirements.

# Contact
Information on how to contact you about issues, bugs, and enhancements to your work

# Credits
Cite credit to anyone you collaborated with, or authors of code you used in your work that is not your own.
