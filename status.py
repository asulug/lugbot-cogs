# from cogs.utils.dataIO import dataIO
# from .utils import checks
# from __main__ import send_cmd_help
# from __main__ import settings as bot_settings
# Sys.
import discord
from redbot.core import commands
import aiohttp
import asyncio
import json
import os
import ast
import socket

import subprocess
from datetime import timedelta

#By p2pFreak
class status(commands.Cog):
    """Check status of services on server."""

    def __init__(self, bot):
        self.bot = bot
        with open('ConfigFiles/statusConfig.json', 'r') as c_json:
            config = json.load(c_json);
        self.config = config;

    @commands.command(pass_context=True, no_pm=True)
    async def status(self, ctx, *service):
        """Check status of services on server."""
        # Call a function on other server
        async def checkSocket(ip,port):
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            r = sock.connect_ex((ip,port))
            sock.close()
            return r
        #await self.bot.send_typing(ctx.message.channel)
        #rpep list of services to check
        #Check processes
        checkAllLocalServices=['nginx','openvpn','sshd','kodi','qbittorrent','vsftpd','Xvnc','mysqld','Xorg','xfce4-session','tvheadend','youtube-dl','bandcamp-dl','udemy-dl','scdl','Frost.Game.Main','RATManager','syncrepo','mase_bc2','Cuberite','TINcft','TINserver','pufferd','Oot','Blockland','node']
        #Check service port
        checkServiceSocketName=['dummy','http-proxy','socks-proxy']
        #Check actual services
        #checkAllLocalServices=['sshd','nginx','mysqld','vsftpd','php-fpm','httpd']
        serviceUp=serviceDown=""
        #Determine and sort arguments
        numbOfArgs=len(service)
        #print(numbOfArgs)
        if numbOfArgs > 0:
            strOfServices=" ".join(service)
            # if strOfServices.find('proc ', 0, 5):
            #     print(service[-1])
                # if strOfServices == "":
                #     for x in checkAllLocalServices:
                #         #Check status of all services
                #         p = subprocess.Popen(["systemctl", "status",x], stdout=subprocess.PIPE)
                #         out, err = p.communicate()
                #         if x in str(out):
                #             serviceUp = serviceUp+":ballot_box_with_check:`"+x+"`\n"
                #         else:
                #             serviceDown = serviceDown+":x:`"+x+"`\n"
                # else:
            #Check only status of services in array
            p = subprocess.Popen(["systemctl", "status",strOfServices], stdout=subprocess.PIPE)
            out, err = p.communicate()
            if 'active (running)' in str(out):
                serviceUp = serviceUp+":ballot_box_with_check:`"+strOfServices+"`\n"
            else:
                serviceDown = serviceDown+":x:`"+strOfServices+"`\n"

        else:
            #Get running/stopped processes
            p = subprocess.Popen(["ps", "-Aw"], stdout=subprocess.PIPE)
            out, err = p.communicate()
            for x in checkAllLocalServices:
                if x in str(out):
                    serviceUp = serviceUp+":ballot_box_with_check:`"+x+"`\n"
                else:
                    serviceDown = serviceDown+":x:`"+x+"`\n"

            # #Check service port
            # for s in checkServiceSocketName:
            #     result = checkSocket(self.config[[s]["ip"]],int(self.config[[s]["port"]]))
            #     if result == 0:
            #         print("Port is open")
            #         serviceUp = serviceUp+":ballot_box_with_check:`"+s+"`\n"
            #     else:
            #         print("Port is not open")
            #         serviceDown = serviceDown+":x:`"+s+"`\n"
        #Uptime calc
        with open('/proc/uptime', 'r') as f:
            uptime_seconds = float(f.readline().split()[0])
            uptime = str(timedelta(seconds = uptime_seconds))

        with open('/etc/hostname', 'r') as f:
            machine = f.readline().split()[0]
        #data = discord.Embed(colour=discord.Colour.yellow())
        data = discord.Embed(colour=0x12b2e7)
        data.set_thumbnail(url="https://orig12.deviantart.net/c6ef/f/2012/362/b/1/arch_linux_badge_by_amai_biscuit-d5phg1r.png")
        if serviceUp!="":
                data.add_field(name="Services:", value=str(serviceUp), inline=True)
        if serviceDown!="":
            data.add_field(name="Services not running:", value=str(serviceDown), inline=True)
        #data.add_field(name="systemctl status:", value=status)
        #data.add_field(name="Plot:", value=("{}\n[Read more...]({})".format("simplePlot", "urlz")))
        data.set_footer(text="Uptime: "+uptime+" | Machine: "+machine)
        await ctx.send(embed=data)

        #Big image, will break soon™
        #find = "._V1_";
        #split_pos = urlPoster.find(find)
        #urlPoster = urlPoster[0:split_pos+5]+".jpg"

        #response = await self.bot.wait_for_message(timeout=20, author=ctx.message.author)
        #if response is None:
        #    pass
        #else:
        #    response = response.content.lower().strip()
        #if response.startswith(("bigpic", "cover", "big", ":eyeglasses:")):
        #    await self.bot.say(urlPoster)
def setup(bot):
    bot.add_cog(status(bot))

